#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/stat.h>
#include <errno.h>
#include <time.h>

#define PORT 8080
#define BUFFER_SIZE 1024

char query[BUFFER_SIZE][BUFFER_SIZE];

int get_request_length (char request[])
{
    return strlen(request)-2;
}

int is_authenticated (char data[])
{
    char user_file_path[BUFFER_SIZE];
    sprintf(user_file_path, "databases/client_database/client_account.txt");

    FILE *user_file;
    user_file = fopen(user_file_path,"r");

    if (user_file == NULL)
    {
        perror("fopen()");
        return EXIT_FAILURE;
    }

    char line_content[BUFFER_SIZE] = {0};
    while (fgets(line_content, sizeof(line_content), user_file) != NULL)
    {
        line_content[strlen(line_content)-1] = '\0';
        if (strcmp(line_content ,data) == 0)
        {
            return 1;
        }
    }
    return 0;
}

int is_granted_access (char data[])
{
    char user_file_path[BUFFER_SIZE];
    sprintf(user_file_path, "databases/client_database/access_account.txt");

    FILE *user_file;
    user_file = fopen(user_file_path,"r");

    if (user_file == NULL)
    {
        perror("fopen()");
        return EXIT_FAILURE;
    }

    char line_content[BUFFER_SIZE] = {0};
    while (fgets(line_content, sizeof(line_content), user_file) != NULL)
    {
        line_content[strlen(line_content)-1] = '\0';
        if (strcmp(line_content ,data) == 0)
        {
            return 1;
        }
    }
    return 0;
}

int is_user_exist (char data[])
{
    char user_file_path[BUFFER_SIZE];
    sprintf(user_file_path, "databases/client_database/client_account.txt");

    FILE *user_file;
    user_file = fopen(user_file_path,"r");

    if (user_file == NULL)
    {
        perror("fopen()");
        return EXIT_FAILURE;
    }

    char line_content[BUFFER_SIZE] = {0};
    while (fgets(line_content, sizeof(line_content), user_file) != NULL)
    {
        char *line_pointer = strtok (line_content, ",");
        char *line_user_data[100];
        int line_user_data_counter = 0;

        while (line_pointer != NULL)
        {
            line_user_data[line_user_data_counter] = line_pointer;
            line_user_data_counter++;
            line_pointer = strtok (NULL, ",");
        }
        if (strcmp(line_user_data[0] ,data) == 0)
        {
            return 1;
        }
    }
    return 0;
}

int is_access_exist (char data[])
{
    char access_file_path[BUFFER_SIZE];
    sprintf(access_file_path, "databases/client_database/access_account.txt");

    FILE *access_file;
    access_file = fopen(access_file_path,"r");

    if (access_file == NULL)
    {
        perror("fopen()");
        return EXIT_FAILURE;
    }

    char line_content[BUFFER_SIZE] = {0};
    while (fgets(line_content, sizeof(line_content), access_file) != NULL)
    {
        line_content[strlen(line_content)-1] = '\0';
        if (strcmp(line_content ,data) == 0)
        {
            return 1;
        }
    }
    return 0;
}

int is_database_exist (char database_name[])
{
    if (database_name[0] == '\0') return 0;

    DIR *database_directory;
    struct dirent *en_a;
    database_directory = opendir("databases");
    while ((en_a = readdir(database_directory)) != NULL)
    {
        if (strcmp(database_name, en_a->d_name) == 0) return 1;
    }
    return 0;
}

int is_table_exist (char table_path[])
{
    FILE *table_file;
    table_file = fopen(table_path , "r");
    if (table_file != NULL)
    {
        return 1;
    }
    return 0;
}

void rewrite_table_columns (char new_table_data[], char path_to_database[], char path_to_table[])
{
    FILE *file;
    FILE *temp;

    file = fopen(path_to_table, "r");
    if (file == NULL) {
        perror("fopen()");
    }

    char path_to_temp_file[BUFFER_SIZE] = {0};
    sprintf(path_to_temp_file, "%stemp.txt", path_to_database);
    temp = fopen(path_to_temp_file, "w");
    if (temp == NULL) {
        perror("fopen()");
    }
    
    int is_first_line = 1;
    char line_content[BUFFER_SIZE];

    while (fgets(line_content , sizeof(line_content) , file)!= NULL)
    {
         if (is_first_line)
         {
            fprintf(temp,"%s\n", new_table_data );
            is_first_line = 0;
        }
        else 
        {
            fprintf(temp,"%s", line_content);
        }
    }
    fflush(file);
    fflush(temp);
    fclose(file);
    fclose(temp);

    remove(path_to_table);
    rename(path_to_temp_file, path_to_table);
}

void write_log(char log[])
{
    FILE *log_file;
    log_file = fopen("log.txt","a");

    if (log_file == NULL) 
    {
        perror("fopen()");
    }
    fprintf(log_file,"%s\n", log);
    fflush(log_file);
    fclose(log_file);
}

int main(int argc, char const *argv[]) {
    int server_fd, new_socket, valread;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char buffer[BUFFER_SIZE] = {0};
    char *hello = "Hello from server";

    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( PORT );

    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0) {
        perror("accept");
        exit(EXIT_FAILURE);
    }

    // determine if user is root or not
    int is_root;

    // authentication data
    char account_data[BUFFER_SIZE] = {0};
    char username[BUFFER_SIZE] = {0};

    recv(new_socket, &is_root, sizeof(is_root), 0);   
    if (is_root == 1)
    {
        strcpy(username, "root");
    }
    else if (is_root == -1){
        char authentication_response[BUFFER_SIZE] = {0};
        valread = read(new_socket ,account_data, BUFFER_SIZE);
        
        for(int i=0; i<strlen(account_data); i++)
        {
            if (account_data[i] == ',')
            {
                break;
            }
            username[i] = account_data[i];
        }
        
        if (is_authenticated(account_data))
        {
            sprintf(authentication_response, "Autentikasi berhasil : %s\n", username);
        }
        else
        {
            sprintf(authentication_response, "Autentikasi gagal\n");
        }

        printf("Autentikasi\n");

        send(new_socket, authentication_response, sizeof(authentication_response), 0);

        FILE *log_file;
        log_file = fopen("log.txt","r");

        if (log_file == NULL) 
        {
        perror("fopen()");
        }

        char string_log[BUFFER_SIZE] = {0};

        int flag = 4;
        while(fgets(string_log, BUFFER_SIZE, log_file)){
            send(new_socket, &flag, sizeof(flag), 0);
            send(new_socket, string_log, sizeof(string_log), 0);
            printf("%s\n", string_log);
        }
        flag =5;
        send(new_socket, &flag, sizeof(flag), 0);


        fflush(log_file);
        fclose(log_file);
        
    }
    else
    {
        char authentication_response[BUFFER_SIZE] = {0};
        valread = read(new_socket ,account_data, BUFFER_SIZE);
        
        for(int i=0; i<strlen(account_data); i++)
        {
            if (account_data[i] == ',')
            {
                break;
            }
            username[i] = account_data[i];
        }
        
        if (is_authenticated(account_data))
        {
            sprintf(authentication_response, "Autentikasi berhasil : %s\n", username);
        }
        else
        {
            sprintf(authentication_response, "Autentikasi gagal\n");
        }

        send(new_socket, authentication_response, sizeof(authentication_response), 0);

    }
    
    // database data
    char used_database[BUFFER_SIZE];
    used_database[0] = '\0';

    while(1)
    {
        char request[BUFFER_SIZE] = {0};
        char response[BUFFER_SIZE] = {0};
        char log[BUFFER_SIZE] = {0};

        // write log
        time_t t = time(NULL);
        struct tm tm = *localtime(&t);
        valread = read(new_socket , request, BUFFER_SIZE);
        sprintf(log, "%d-%02d-%02d %02d:%02d:%02d:%s:%s", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec, username, request);
        write_log(log);

        printf("%s", request);

        char *CREATE_USER = strstr(request, "CREATE USER");         
        char *CREATE_DATABASE = strstr(request, "CREATE DATABASE"); 
        char *USE= strstr(request, "USE");                          
        char *GRANT_PERMISSION= strstr(request, "GRANT PERMISSION");
        char *CREATE_TABLE= strstr(request, "CREATE TABLE");        
        char *DROP= strstr(request, "DROP");                        
        char *INSERT_INTO= strstr(request, "INSERT INTO");          
        char *UPDATE= strstr(request, "UPDATE");                    
        char *SELECT= strstr(request, "SELECT");                    
        char *DELETE= strstr(request, "DELETE");                    
        char *EXIT = strstr(request, "EXIT");                       

        if (CREATE_USER)
        {
            // CREATE USER khonsu IDENTIFIED BY khonsu123;
            
            if (is_root)
            {
                char username_data[BUFFER_SIZE] = {0};
                char password_data[BUFFER_SIZE] = {0};
                
                int space_counter = 0;
                int username_data_counter = 0;
                int password_data_counter = 0;

                for (int i=0 ; i<get_request_length(request) ; i++)
                {
                    if (request[i] == ' ')
                    {
                        space_counter++;
                    }
                    else if (space_counter == 2)
                    {
                        username_data[username_data_counter] = request[i];
                        username_data_counter++;
                    }
                    else if (space_counter == 5)
                    {
                        password_data[password_data_counter] = request[i];
                        password_data_counter++;
                    }
                    else
                    {
                        continue;
                    }
                }
                if (is_user_exist(username_data))
                {
                    sprintf(response, "User %s telah terdaftar\n", username_data);
                }
                else
                {
                    char user_file_path[BUFFER_SIZE];
                    sprintf(user_file_path, "databases/client_database/client_account.txt");

                    FILE *user_file;
                    user_file = fopen(user_file_path,"a");

                    if (user_file == NULL)
                    {
                        perror("fopen()");
                        return EXIT_FAILURE;
                    }
                    fprintf(user_file,"%s,%s\n", username_data, password_data );
                    fflush(user_file);
                    fclose(user_file);
                    
                    sprintf(response, "User berhasil ditambahkan\n"); 
                }
            }
            else
            {
                sprintf(response, "User bukan root tidak dapat mengakses fungsi ini\n");
            }
            send(new_socket, response, sizeof(response), 0);
        }

        else if (CREATE_DATABASE)
        {
            // CREATE DATABASE database1

            char database_name[BUFFER_SIZE] = {0};
            char database_path[BUFFER_SIZE] = {0};
            int space_counter = 0;
            int database_counter = 0;
            
            // detect the first letter of the database name
            for (int i=strlen(request); i>0; i--)
            {
                if (request[i] == ' ')
                {
                    space_counter = i;
                    break;
                }
            }
            
            // get database name
            for (int j=space_counter+1; j<get_request_length(request); j++)
            {
                database_name[database_counter] = request[j];
                database_counter++;
            }
            
            // generate database path
            sprintf(database_path, "databases/%s", database_name);

            // make directory
            int is_not_success = mkdir(database_path, 0777);

            if (is_not_success)
            {
                perror("mkdir()");
                return EXIT_FAILURE;
            }

            // add user permission to the database
            if (!is_root)
            {
                char permission[BUFFER_SIZE] = {0};
                char username[BUFFER_SIZE] = {0};

                for (int i=0; i<strlen(account_data); i++)
                {
                    if (account_data[i] == ',') break;
                    username[i] = account_data[i];
                }

                sprintf(permission, "%s,%s", database_name, username);
                
                FILE *access_file;
                access_file= fopen("databases/client_database/access_account.txt","a");
                if (access_file == NULL)
                {
                    perror("fopen()");
                    return EXIT_FAILURE;
                }

                fprintf(access_file,"%s\n", permission);
                fflush(access_file);
                fclose(access_file);
            }

            sprintf(response, "Sukses membuat database : %s\n", database_name);
            send(new_socket, response, sizeof(response), 0);
        }

        else if (USE)
        {

            char database_name[BUFFER_SIZE] = {0};
            int space_counter = 0;
            int database_counter = 0;
            
            // detect the first letter of the database name
            for (int i=strlen(request); i>0; i--)
            {
                if (request[i] == ' ')
                {
                    space_counter = i;
                    break;
                }
            }

            // get database name
            for (int j=space_counter+1; j<get_request_length(request); j++)
            {
                database_name[database_counter] = request[j];
                database_counter++;
            }

            // check if database existed and user is allowed to access database
            if (is_database_exist(database_name))
            {
                int is_allowed = 0;
               
                if (!is_root)
                {
                    char permission[BUFFER_SIZE];
                    char username[BUFFER_SIZE] = {0};
                    
                    for (int i=0; i<strlen(account_data); i++)
                    {
                        if (account_data[i] == ',') break;

                        username[i] = account_data[i];
                    }

                    sprintf(permission, "%s,%s", database_name, username);
                    if (is_granted_access(permission)) is_allowed = 1;
                }
                if (is_allowed || is_root)
                {
                    // add database name to used database
                    used_database[0]='\0';
                    strcpy(used_database, database_name);
                    sprintf(response, "%s berhasil digunakan\n", used_database);
                }
                else
                {
                    sprintf(response, "Autentikasi gagal untuk database %s\n", used_database);
                }
            }
            else 
            {
                sprintf(response, "%s tidak ditemukan\n", used_database);
            }

            send(new_socket, response, sizeof(response), 0);
        }

        else if (GRANT_PERMISSION)
        {
            // GRANT PERMISSION database1 INTO user1;

            if (is_root)
            {
                char username_data[BUFFER_SIZE] = {0};
                char database_name_data[BUFFER_SIZE] = {0};
                
                int space_counter = 0;
                int username_data_counter = 0;
                int database_name_data_counter = 0;

                for (int i=0 ; i<get_request_length(request) ; i++)
                {
                    if (request[i] == ' ')
                    {
                        space_counter++;
                    }
                    else if (space_counter == 2)
                    {
                        database_name_data[database_name_data_counter] = request[i];
                        database_name_data_counter++;
                    }
                    else if (space_counter == 4)
                    {
                        username_data[username_data_counter] = request[i];
                        username_data_counter++;
                    }
                    else
                    {
                        continue;
                    }
                }

                int user_exist = is_user_exist(username_data);
                int database_exist = is_database_exist(database_name_data);

                char permission[BUFFER_SIZE] = {0};
                sprintf(permission, "%s,%s", database_name_data, username_data);
                if (is_access_exist(permission))
                {
                    sprintf(response, "Akses kepada %s telah terdaftar sebelumnya\n", username_data);
                }
                else if (!user_exist && !database_exist)
                {
                    sprintf(response, "Akun %s dan Database %s tidak ditemukan\n", username_data, database_name_data);
                }
                else if (!user_exist)
                {
                    sprintf(response, "Akun %s belum terdaftar\n", username_data);
                }
                else if (!database_exist)
                {
                    sprintf(response, "Database %s belum terdaftar\n", database_name_data);
                }
                else 
                {
                    char access_file_path[BUFFER_SIZE];
                    sprintf(access_file_path, "databases/client_database/access_account.txt");

                    FILE *access_file;
                    access_file = fopen(access_file_path,"a");

                    if (access_file == NULL)
                    {
                        perror("fopen()");
                        return EXIT_FAILURE;
                    }
                    fprintf(access_file,"%s,%s\n", database_name_data, username_data);
                    fflush(access_file);
                    fclose(access_file);
                    
                    sprintf(response, "Akses database berhasil ditambahkan\n"); 
                }
            }
            else
            {
                sprintf(response, "User bukan root tidak dapat mengakses fungsi ini\n");
            }
            send(new_socket, response, sizeof(response), 0);
        }

        else if (DROP)
        {
            if (used_database[0] == '\0')
                sprintf(response, "Tidak ada database yang sedang digunakan\n");
            else
            {
                // determine drop database or table
                char *is_database = strstr(request, "DATABASE");
                char *is_table = strstr(request, "TABLE");

                // get the database or table value to be dropped
                int space_counter = 0;
                char data_name[BUFFER_SIZE] = {0};
                int data_name_counter = 0;

                for (int i=0; i<get_request_length(request); i++)
                {
                    if (request[i] == ' ')
                    {
                        space_counter++;
                    }
                    else if (request[i] == ';' && space_counter == 2)
                    {
                        continue;
                    } 
                    else if (space_counter == 2 && request[i] != ' ')
                    {
                        data_name[data_name_counter] = request[i];
                        data_name_counter++;
                    }
                }

                // delete data
                int is_exist = 0;
                int is_delete_success = -1;
                if (is_database) 
                {
                    // buat database path
                    char database_path[BUFFER_SIZE] = {0};
                    sprintf(database_path, "databases/%s", data_name);

                    // check database existence
                    is_exist = is_database_exist(data_name);

                    // delete database
                    if (is_exist)
                    {
                        is_delete_success = rmdir(database_path);
                    }
                    else
                    {
                        sprintf(response, "direktori database tidak ada : %s\n", database_path);
                    }
                    
                }
                else if (is_table)
                {
                    // buat table path
                    char table_path[BUFFER_SIZE] = {0};
                    sprintf(table_path, "databases/%s/%s.txt", used_database, data_name);

                    // check table existence
                    is_exist = is_table_exist(table_path);

                    // delete table
                    if (is_exist) 
                    {
                        is_delete_success = remove(table_path);
                    } 
                    else 
                    {
                        sprintf(response, "tabel tidak ada : %s\n", table_path);
                    }
                }

                // check is success and append response
                if (is_delete_success == 0)
                {
                    sprintf(response, "%s berhasil di delete\n", data_name);
                }
                else 
                {
                    sprintf(response, "%s tidak berhasil di delete\n", data_name);
                }
            }

            send(new_socket, response, sizeof(response), 0);
        }

        else if (CREATE_TABLE)
        {
            // CREATE TABLE table2 (kolom1 string, kolom2 int);

            if (used_database[0] == '\0')
                sprintf(response, "Tidak ada database yang sedang digunakan\n");
            else
            {
                char table_name[BUFFER_SIZE] = {0};
                char table_column_data[BUFFER_SIZE] = {0};
                
                int space_counter = 0;
                int table_name_counter = 0;
                int table_column_data_counter = 0;

                for (int i=0 ; i<get_request_length(request) ; i++)
                {
                    if (request[i] == ' ')
                    {
                        space_counter++;
                    }
                    if (request[i] == '(' || request[i] == ')')
                    {
                        continue;
                    }
                    if (space_counter == 2 && request[i] != ' ')
                    {
                        table_name[table_name_counter] = request[i];
                        table_name_counter++;
                    }
                    if (space_counter >= 3)
                    {
                        if (
                            (request[i] == ' ' && space_counter == 3) || 
                            (request[i] == ' ' && request[i-1] == ','))
                        {
                            continue;
                        }
                        table_column_data[table_column_data_counter]=request[i]; 
                        table_column_data_counter++;
                    }
                }

                char path_to_table[BUFFER_SIZE] = {0};
                sprintf(path_to_table, "databases/%s/%s.txt", used_database, table_name);

                if (is_table_exist(path_to_table))
                {
                    sprintf(response, "Gagal membuat tabel. Tabel %s sudah ada pada Database %s\n", table_name, used_database); 
                }
                else
                {
                    FILE *table_file;
                    table_file = fopen(path_to_table , "a");
                    if (table_file == NULL)
                    {
                        sprintf(response, "direktori tidak ada : %s", path_to_table); 
                    }

                    fprintf(table_file,"%s\n\n", table_column_data );
                    fflush(table_file);
                    fclose(table_file);
                    
                    sprintf(response, "Tabel %s pada %s berhasil dibuat\n", table_name, used_database); 
                }
            } 



            send(new_socket, response, sizeof(response), 0);
        }
        
        else if (INSERT_INTO)
        {

            //INSERT INTO [nama_tabel] ([value], ...);
            //INSERT INTO table1 (‘value1’, 2, ‘value3’, 4);

            if (used_database[0] == '\0')
                sprintf(response, "Tidak ada database yang sedang digunakan\n");
            else
            {
                char table_name[BUFFER_SIZE] = {0};
                char column_value[BUFFER_SIZE] = {0};

                int space_counter = 0;
                int table_name_counter = 0;
                int column_value_counter = 0;

                for (int i=0 ;i<get_request_length(request) ;i++)
                {
                    if (request[i] == ' ')
                    {
                        space_counter++;
                    }
                    if (request[i] == '(' || request[i] == ')' || request[i] == '\'')
                    {
                        continue;
                    }
                    if (space_counter == 2 && request[i] != ' ')
                    {
                        table_name[table_name_counter] = request[i];
                        table_name_counter++;
                    }
                    if (space_counter >= 3)
                    {
                        if (
                            (request[i] == ' ' && space_counter == 3) || 
                            (request[i] == ' ' && request[i-1] == ','))
                        {
                            continue;
                        }
                        column_value[column_value_counter]=request[i]; 
                        column_value_counter++;
                    }
                }
                
                char path_to_table[BUFFER_SIZE] = {0};
                sprintf(path_to_table, "databases/%s/%s.txt", used_database, table_name);
                
                
                FILE *table_file;
                table_file = fopen(path_to_table,"a");
                if (table_file == NULL)
                {
                    sprintf(response, "direktori tidak ada : %s", path_to_table); 
                }
                
                fprintf(table_file,"%s\n", column_value );
                fflush(table_file);
                fclose(table_file);
                
                sprintf(response, "Data berhasil ditambahkan\n"); 
                
                
            }
            send(new_socket, response, sizeof(response), 0);
        }

        else if (UPDATE)
        {
            
            // UPDATE [nama_tabel] SET [nama_kolom]=[value];
            // UPDATE table1 SET kolom1=’new_value1’;
            
            if (used_database[0] == '\0')
            {   
                sprintf(response, "Tidak ada database yang sedang digunakan\n");
            }
            else
            {
                char table_name[BUFFER_SIZE] = {0};
                char old_column_name[BUFFER_SIZE] = {0};
                char new_column_name[BUFFER_SIZE] = {0};

                int space_counter = 0;
                int table_name_counter = 0;
                int old_column_name_counter = 0;
                int new_column_name_counter = 0;

                int is_new_column_value = 0;

                // get the old column name and the new column name value
                for (int i=0; i<get_request_length(request); i++)
                {
                    if (request[i] == ' ')
                    {
                        space_counter++;
                    }
                    if (request[i] == '=' || request[i] == '\'' || request[i] == ';')
                    {
                        is_new_column_value = 1;
                        continue;
                    }
                    if (space_counter == 1 && request[i] != ' ')
                    {
                        table_name[table_name_counter] = request[i];
                        table_name_counter++;
                    }
                    if (space_counter == 3 && request[i] != ' ')
                    {
                        if (!is_new_column_value)
                        {
                            old_column_name[old_column_name_counter] = request[i];
                            old_column_name_counter++;
                        }
                        else
                        {
                            new_column_name[new_column_name_counter] = request[i];
                            new_column_name_counter++;
                        }
                    }
                }

                // determine path to table and path to database
                char path_to_table[BUFFER_SIZE] = {0};
                char path_to_database[BUFFER_SIZE] = {0};

                sprintf(path_to_table, "databases/%s/%s.txt", used_database, table_name);
                sprintf(path_to_database, "databases/%s/", used_database);

                FILE *table_file;
                table_file = fopen(path_to_table , "r");
                if (table_file == NULL)
                {
                    sprintf(response, "direktori tidak ada %s", path_to_table); 
                }
                else
                {
                    
                    int is_comma = 1;
                    int is_column_name_found = 0;
                    int data_column_name_counter = 0;
                    char data_column_name[BUFFER_SIZE] = {0};
                    
                    // algorithm for replacing old column name value with new column name value
                    int lower_bound = 0;
                    int upper_bound = 0;

                    char table_data[BUFFER_SIZE] = {0};

                    fscanf(table_file, "%[^\n]", table_data);
                    
                    for (int j=0; j<strlen(table_data); j++)
                    {
                        if (table_data[j] == ',')
                        {
                            is_comma = 1;
                            continue;
                        }
                        else if (table_data[j] == ' ')
                        {
                            is_comma = 0;

                            if (strcmp(old_column_name, data_column_name)==0)
                            {
                                is_column_name_found = 1;
                                upper_bound = j;
                                lower_bound = j-(strlen(old_column_name));
                            }
                            memset(data_column_name, 0, BUFFER_SIZE);
                            data_column_name_counter = 0;
                           
                        }
                        else if (is_comma == 1 && table_data[j]!= ' ')
                        {
                            data_column_name[data_column_name_counter] = table_data[j];
                            data_column_name_counter++;
                        }
                        else 
                        {
                            continue;
                        }
                    }

                    if (is_column_name_found)
                    {
                        char new_table_data[BUFFER_SIZE] = {0};

                        for (int x=0; x<lower_bound; x++)
                        {
                            new_table_data[x] = table_data[x];
                        }
                        for (int lb=lower_bound; lb<lower_bound+strlen(new_column_name); lb++)
                        {
                            new_table_data[lb] = new_column_name[lb-lower_bound];
                        }
                        for (int ub=upper_bound; ub<strlen(table_data); ub++)
                        {
                            new_table_data[strlen(new_column_name) + lower_bound + (ub-upper_bound)] = table_data[ub];
                        }

                        // sprintf(response, "%s\n", new_table_data);
                        
                        rewrite_table_columns(new_table_data, path_to_database, path_to_table);
                        sprintf(response, "Tabel berhasil diperbarui\n"); 
                    } 
                    else 
                    {
                        sprintf(response, "Nama kolom tidak ditemukan\n"); 
                    }
                    
                }
                
                send(new_socket, response, sizeof(response), 0);
            }
        }

        else if (SELECT)
        {
            // SELECT kolom1, kolom2 FROM table1;
            // SELECT * FROM table1;
            
            if (used_database[0] == '\0')
            {   
                sprintf(response, "Tidak ada database yang sedang digunakan\n");
            }
            else
            {
                char table_name[BUFFER_SIZE] = {0};
                int table_space_counter = 0;
                int table_counter = 0;

                // determine table name
                for (int i=strlen(request); i>0; i--)
                {
                    if (request[i] == ' ')
                    {
                        table_space_counter = i;
                        break;
                    }
                }
                for (int j=table_space_counter+1; j<get_request_length(request); j++)
                {
                    table_name[table_counter] = request[j];
                    table_counter++;
                }

                char path_to_table[BUFFER_SIZE] = {0};
                sprintf(path_to_table, "databases/%s/%s.txt", used_database, table_name);

                FILE *table_file;
                table_file = fopen(path_to_table , "r");

                
                char select_result[BUFFER_SIZE] = {0};

                if (table_file == NULL)
                {
                    sprintf(response, "direktori tidak ada %s\n", path_to_table); 
                }
                else
                {
                    //determine if the select is by column or *
                    char *is_select_all = strstr(request, "*");
                    if(is_select_all)
                    {
                        int is_first_line = 0;
                        char line_content[BUFFER_SIZE];

                        while (fgets(line_content , sizeof(line_content) , table_file)!= NULL)
                        {
                            if(is_first_line<2){
                                is_first_line++;
                            }
                            else{
                                strcat(select_result, line_content);
                            }
                        }
                    }
                    else
                    {
                        char table_data[BUFFER_SIZE] = {0};
                        fscanf(table_file, "%[^\n]", table_data);

                        // determine array of column stored in table
                        
                        int is_comma = 1;
                        // how many column
                        int data_column_name_count = 0;
                        // iterate over column data
                        int data_column_name_counter = 0;

                        char data_column_name[100][BUFFER_SIZE] = {'\0'};

                        for (int j=0; j<strlen(table_data); j++)
                        {
                            if (table_data[j] == ',')
                            {
                                is_comma = 1;
                                data_column_name_count++;                          
                            }
                            else if (table_data[j] == ' ')
                            {
                                data_column_name_counter = 0;
                                is_comma = 0;
                            }
                            else if (is_comma == 1 && table_data[j]!= ' ')
                            {
                                data_column_name[data_column_name_count][data_column_name_counter] = table_data[j];
                                data_column_name_counter++;
                            } else {
                                continue;
                            }
                        }

                        // determine request column index

                        // store requested column index compared to database st fr 0
                        int column_index_data[BUFFER_SIZE] = {0};
                        int column_index_data_counter = 0;

                        int request_space_counter = 0;
                        char request_column_name[BUFFER_SIZE] = {0};
                        int request_column_name_counter = 0;
                        
                        for (int i=0; i<get_request_length(request); i++)
                        {
                            if (request[i] == ',' || request_space_counter == 2)
                            {
                                // add column index to array
                                for(int j=0; j<=data_column_name_count; j++){
                                    if(strcmp(data_column_name[j], request_column_name) == 0)
                                    {
                                        column_index_data[column_index_data_counter] = j;
                                        column_index_data_counter++;
                                    }
                                }

                                // reset array
                                memset(request_column_name, 0, BUFFER_SIZE);
                                request_column_name_counter=0;
                                continue;
                            }
                            else if (request[i] == ' ' && request[i-1] == ',' && i > 0)
                            {
                                continue;
                            }
                            else if (request[i] == ' ')
                            {
                                request_space_counter++;
                            }
                            else if (request_space_counter == 1)
                            {
                                request_column_name[request_column_name_counter] = request[i];
                                request_column_name_counter++;
                            }
                        }
                        
                        
                        
                        // print content
                        int is_first_line = 0;
                        char line_content[BUFFER_SIZE];

                        while (fgets(line_content , sizeof(line_content) , table_file)!= NULL)
                        {
                            if(is_first_line<2){
                                is_first_line++;
                            }
                            else{
                                char temporary_line_value[BUFFER_SIZE] = {0};

                                char *line_pointer = strtok (line_content, ",");
                                char *line_column_data[100];
                                int line_column_data_counter = 0;

                                while (line_pointer != NULL)
                                {
                                    line_column_data[line_column_data_counter] = line_pointer;
                                    line_column_data_counter++;
                                    line_pointer = strtok (NULL, ",");
                                }
                            
                                line_column_data[line_column_data_counter-1][strlen(line_column_data[line_column_data_counter-1])-1] = '\0'; 

                                for(int i=0; i<column_index_data_counter; i++)
                                {
                                    strcat(temporary_line_value, line_column_data[column_index_data[i]]);
                                    if(i!=column_index_data_counter-1){
                                        strcat(temporary_line_value,",");
                                    }
                                }
                                strcat(select_result, temporary_line_value);
                                strcat(select_result, "\n");
                            }
                        }
                        

                        
                    }

                    fflush(table_file);
                    fclose(table_file);
                    sprintf(response, "%s", select_result);   
                }
               
            }
            send(new_socket, response, sizeof(response), 0);
                //printf("%s %s\n", table_name, data_details);
        }
        
        else if (DELETE)
        {
            
            // DELETE FROM [nama_tabel];
            // DELETE FROM table1;

            if (used_database[0]=='\0')
            {
                sprintf(response, "NO DATABASE IN USE!!!\n");
            }
            else
            {

                char table_name[BUFFER_SIZE] = {0};
                int space_counter = 0;
                int table_name_counter = 0;
                
                // detect the first letter of the table name
                for (int i=strlen(request); i>0; i--)
                {
                    if (request[i] == ' ')
                    {
                        space_counter = i;
                        break;
                    }
                }

                // get table name
                for (int j=space_counter+1; j<get_request_length(request); j++)
                {
                    table_name[table_name_counter] = request[j];
                    table_name_counter++;
                }
                
                 // determine path to table and path to table
                char path_to_table[BUFFER_SIZE] = {0};
                sprintf(path_to_table, "databases/%s/%s.txt", used_database, table_name);
                

                FILE *table_file;
                table_file = fopen(path_to_table , "r");
                if (table_file == NULL)
                {
                    sprintf(response, "direktori tabel tidak ada %s", path_to_table); 
                }
                else
                {
                    // delete data from table by creating a new file and then putting the column names on it
                    char table_data[BUFFER_SIZE];
                    fgets(table_data , sizeof(table_data) , table_file);
                    remove(path_to_table);

                    table_file= fopen(path_to_table,"a");
                    fprintf(table_file, "%s\n", table_data);

                    fflush(table_file);
                    fclose(table_file);
                    sprintf(response, "Sukses menghapus data tabel\n");
                }
            }
            send(new_socket, response, sizeof(response), 0);
        }
        
        else if (EXIT)
        {
            break;
        }

        else
        {
            sprintf(response, "Command yang diinputkan salah\n");
            send(new_socket, response, sizeof(response), 0);
        }
    } 

    return 0;
}
