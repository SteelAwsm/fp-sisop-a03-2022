#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#define PORT 8080

#define BUFFER_SIZE 1024
  
int main(int argc, char const *argv[]) {
    struct sockaddr_in address;
    int sock = 0, valread;
    struct sockaddr_in serv_addr;
    char *hello = "Hello from client";
    char buffer[1024] = {0};
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        return -1;
    }
  
    memset(&serv_addr, '0', sizeof(serv_addr));
  
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
      
    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }
  
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("\nConnection Failed \n");
        return -1;
    }

    int is_dump = -1;
    send(sock, &is_dump, sizeof(is_dump), 0);

    char account_data[BUFFER_SIZE] = {0};
    char authentication_response[BUFFER_SIZE] = {0};

    sprintf(account_data, "%s,%s", argv[2], argv[4]);
    send(sock , account_data, strlen(account_data) , 0 );
        
    valread = read(sock, authentication_response, BUFFER_SIZE);

    printf("%s", authentication_response);

    //find how much lenth in log file

    int flag;
    char string_log[BUFFER_SIZE];

    while(1){
        valread = read(sock, &flag, sizeof(flag));
        if(flag == 5){
            break;
        }
        valread = read(sock, string_log, sizeof(string_log));
        printf("%s\n", string_log);
        fgets(string_log, BUFFER_SIZE, stdout);
        memset(string_log, 0, BUFFER_SIZE);
    }

    
    
}
